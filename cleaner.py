
def leggi_csv(file, sep):
    """carica un file csv in un data frame"""
    import pandas as pd
    df = pd.read_csv(file, header=None, sep=sep)
    return df

def df_to_matrix(df):
    """converte dataframe a matrice"""
    import pandas as pd
    return df.values

def csv_to_matrix(file, sep):
    """Prende un csv e lo converte in matrix"""
    import pandas as pd
    df = pd.read_csv(file, header=None, sep=sep)
    return df.values

def standard_matrix(x):
    """Trasforma matrice con valori standardizzati"""
    from sklearn import preprocessing
    from sklearn.preprocessing import LabelEncoder, OneHotEncoder
    df_scaler = preprocessing.MinMaxScaler(feature_range=(0, 1))
    df = df_scaler.fit_transform(x)
    return df

def matrix_to_df(matrix):
    """converte dataframe a matrice"""
    import pandas as pd
    return pd.DataFrame(matrix)

def reshape(x,colonne):
    """converte matrice (n,m) in una matrice (n/k, k)"""
    return x.reshape(int((x.shape[0]*x.shape[1])/colonne),colonne)

def append_matrix(x,y):
    """appende per colonna due matrici"""
    import numpy as np
    return np.append(x,y, axis=0)

def divisori(num):
    """calcola divisori di un numero"""
    for i in range(1,num+1):
        if (num%i==0):
            print(i)
    
def estrai_target(y):
    """dato un dataset (n,m) crea un train (n,m-1) e un test (n,1) ultima colonna"""
    x=y[:,0:(y.shape[1]-1)]
    y=y[:,(y.shape[1]-1)]
    return x,y

def differenza(x,y):
    """ data matrice x (n,m) e vettore y (n,1) calcola nuova matrice (n,m) dove x[i,j]=(x[i,j]-y[i]) / y[i]"""
    for i in range(0,x.shape[0]):
        for j in range(0,x.shape[1]):
            x[i,j]=(x[i,j]-y[i])/y[i]
    return x

def differenza2(x):
    y=x
    k2=x.columns
    for i in range(0,len(k2)-1):
       y[k2[i]]=x[k2[i]]-x[k2[i+1]]
    return y

def duplicati_e_nan(x):
    """ dato un dataframe, toglie le righe duplicate e le righe con nan"""
    x=x.dropna()
    x=x.drop_duplicates()
    return x

def dividi_dataset(x,var, perc):
    from sklearn.model_selection import train_test_split
    X=x.loc[:, x.columns != var]
    cl1=x[var]
    x_train, x_test, y_train, y_test = train_test_split(X, cl1, test_size = perc)
    return x_train, x_test, y_train, y_test

def dividi_dataset2(x, perc):
    X=x[:,0:x.shape[1]-1]
    cl1=x[:,x.shape[1]-1]
    r=round(x.shape[0]*(1-perc))
    x_train=X[0:r]
    x_test=cl1[0:r]
    y_train=X[r:]
    y_test=cl1[r:]

    return x_train, x_test, y_train, y_test



    

